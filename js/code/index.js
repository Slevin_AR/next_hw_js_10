let calcValue = "0";
let firstValue = "";
let calcFn = null;
let memory = "0";

showOnDisplay(calcValue);


document.getElementById("memory").innerHTML= "";

document.addEventListener("click", (e) => {
    const element = e.target;
    const isButton = element.classList.contains("button");
    const buttonValue = element.value;
    const isMath = element.classList.contains("pink");
    const isMemory = element.classList.contains("gray");
    if (!isButton) {
        return;
    }

    // handles "clear" button => "C"
    const isClearButton = buttonValue === "C";
    if (isClearButton) {
        calcValue = "0";
    }

    // handles "dot"
    const isDotButton = buttonValue === ".";
    if (isDotButton){
        const isDotExist = calcValue.includes(".");
        let value = buttonValue;

        if (isDotButton && isDotExist){
            value = '';
        }

        calcValue = calcValue + value;
    }

    // Handles numbers
    const isNumberButton = buttonValue.match(/\d/);
    if (isNumberButton) {
        if (calcValue === '0') {
            calcValue = buttonValue;
        } else {
            calcValue = calcValue + buttonValue;
        }
    }
    // Handles 
    
    if(isMath){
        firstValue = calcValue;
        if (buttonValue === '+') {
            calcFn = sum;
        }
        if (buttonValue === '-') {
            calcFn = sub;
        }
        if (buttonValue === '*') {
            calcFn = mult;
        }
        if (buttonValue === '/') {
            calcFn = divi;
        }
        calcValue = "0";     
    }

    const isEquealButton = element.classList.contains("orange");
    if (isEquealButton) {
        calcValue = calcFn(parseFloat(firstValue), parseFloat(calcValue));
        firstValue = '';
        calcFn = null;
    }

    const equealButton = document.querySelector('.button.orange');
    if (calcFn !== null && calcValue !== '') {
        equealButton.disabled = false;
    } else {
        equealButton.disabled = true;
    }

    if(isMemory){
        if(buttonValue === "m+"){
            memory = calcValue;
            document.getElementById("memory").innerHTML= "M";
        }
        if(buttonValue === "m-"){
            memory = "0";
            document.getElementById("memory").innerHTML= "";
        }
        if(buttonValue === "mrc"){
            calcValue = memory;
        }
    }

    showOnDisplay(calcValue);
});
    
function sum (a, b) {
    return a + b;
}

function sub (a, b) {
    return a - b;
}

function mult (a, b) {
    return a * b;
}

function divi (a, b) {
    return a / b;
}

function showOnDisplay (number) {
    document.querySelector(".display > input").value = number;
}

